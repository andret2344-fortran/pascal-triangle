program triangle
	integer(kind = 2) iw
	dimension iw(12, 23)
	print *, 'Type amount of levels'
	read(*, *) n
	print *, 'Pascal triangle for', n, 'levels'
	do i = 1, 12
		do j = 1, 23
			iw(i, j) = 0
		end do
	end do
	iw(1, n) = 1
	iw(n, 1) = 1
	iw(n, 2 * n - 1) = 1
	do i = 1, n
		do j = 2, 2 * n - 2
			if (.not.i==1) then
				iw(i, j) = iw(i - 1, j - 1) + iw(i - 1, j + 1)
			end if
		end do
	end do
	do i = 1, n
		do j = 1, 2 * n - 1
			if (iw(i, j)==0) then
				write (*, '(A,X)', advance = 'no') '  '
			else
				write (*, '(I3,X)', advance = 'no') iw(i, j)
			end if
		end do
		write(*, *) ' '
	end do
end program triangle
